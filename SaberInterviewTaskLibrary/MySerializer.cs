﻿using System.Buffers;
using System.Text;
using SerializerTests.Interfaces;
using SerializerTests.Nodes;

namespace SerializerTests.Implementations
{
    //Specify your class\file name and complete implementation.
    public class MySerializer : IListSerializer
    {
        private const int NullMarker = -1;
        private const int DataAndPointersSeparator = -2;
        //Some random value that can be changed according to data 
        private const int DataBufferSize = 512;
        
        private readonly byte[] _dataStorageBuffer = new byte[DataBufferSize];
        private readonly ArrayPool<byte> _arrayPool = ArrayPool<byte>.Shared;

        //Use Unicode by default
        private readonly Encoder _encoder = Encoding.Unicode.GetEncoder();
        
        private static readonly byte[] DataAndPointersBytesSeparator = {254, 255, 255, 255 };

        //the constructor with no parameters is required and no other constructors can be used.
        public MySerializer()
        {
            //...
        }

        public Task<ListNode> DeepCopy(ListNode head)
        {
            var copiesDictionary = new Dictionary<ListNode, ListNode>();

            return Task.FromResult(Copy(head));
            
            ListNode Copy(ListNode node, ListNode prev = null)
            {
                if (node is null)
                {
                    return null;
                }

                if (copiesDictionary.TryGetValue(node, out var copy))
                {
                    return copy;
                }

                copy = new ListNode {Data = node.Data};
                copiesDictionary.Add(node, copy);

                if (prev is not null)
                {
                    copy.Previous = prev;
                }
                
                copy.Next = Copy(node.Next, copy);
                copy.Random = Copy(node.Random);
                return copy;
            }
        }
        
        /// <summary>
        /// Store nodes in a sequential order, data first, random pointer indexes after in the same order 
        /// Random management flow: 
        /// 1) Add into first dict with seq counter
        /// 2) Check if current node is in dict2 
        /// 3.1) If 2 == true -> take all node seq numbers from dict2 value by current node, for all set bytes[value] = current node, remove current node from dict2
        /// 3.2) If 2 == false -> do nothing
        /// -----------------------Try to set random
        /// 4) Check if random exists
        /// 5.1) If 4 == false -> bytes.Add(-1); break
        /// 5.2) if 4 == true -> continue
        /// 6) Check if random node is in dict1
        /// 7.1) If 6 == true -> set bytes.Add(dict1.Value)
        /// 7.2) If 6 == false -> add randomNode to dict2 with value current seq number (or extend already existing list), bytes.Add(-1)
        ///
        /// Final view: {x(4bytes)->data(x bytes)}...{-1 (separator)}{randomPointerIndex(4bytes(-1 if null))}...
        /// </summary>
        /// <param name="head">Node </param>
        /// <param name="s"></param>
        /// <returns></returns>
        public Task Serialize(ListNode head, Stream s)
        {
            //Dictionary for nodes sequential id
            var nodesSeqIds = new Dictionary<ListNode, int>();

            //Dictionary for sequential ids of the nodes which refer target node as random
            var referredNodes = new Dictionary<ListNode, List<int>>();

            //Sequential list of indexes of nodes' random pointers
            var randoms = new List<int>();

            //Current node index
            var currentIndex = 0;
            
            Span<byte> intStorageSpan = stackalloc byte[4];

            int dataBytesUsed;
            int dataCharsUsed;
            while (head is not null)
            {
                if (head.Data is null)
                {
                    s.Write(intStorageSpan);
                }
                else
                {
                    var dataLength = _encoder.GetByteCount(head.Data, true);
                    BitConverter.TryWriteBytes(intStorageSpan, dataLength);
                    s.Write(intStorageSpan);

                    var dataStorageBufferSpan = new Span<byte>(_dataStorageBuffer);
                
                    var dataIndex = 0;
                    var completed = false;
                    
                    while (!completed)
                    {
                        _encoder.Convert(head.Data.AsSpan(dataIndex), dataStorageBufferSpan, false,
                            out dataCharsUsed, out dataBytesUsed, out completed);
                        s.Write(dataStorageBufferSpan[..dataBytesUsed]);
                        dataIndex += dataCharsUsed;
                    }
                    
                }
                
                nodesSeqIds.Add(head, currentIndex);
                if (referredNodes.TryGetValue(head, out var referrers))
                {
                    foreach (var referrerIndex in referrers)
                    {
                        randoms[referrerIndex] = currentIndex;
                    }

                    referredNodes.Remove(head);
                }

                if (head.Random is not null)
                {
                    if (nodesSeqIds.TryGetValue(head.Random, out var randomIndex))
                    {
                        randoms.Add(randomIndex);
                    }
                    else
                    {
                        if (!referredNodes.TryGetValue(head.Random, out var randomNodeReferrers))
                        {
                            randomNodeReferrers = new List<int>();
                            referredNodes.Add(head.Random, randomNodeReferrers);
                        }
                        randomNodeReferrers.Add(currentIndex);
                        randoms.Add(NullMarker);
                    }
                }
                else
                {
                    randoms.Add(NullMarker);
                }

                currentIndex++;
                head = head.Next;
            }

            s.Write(DataAndPointersBytesSeparator);
            foreach (var randomPointer in randoms)
            {
                BitConverter.TryWriteBytes(intStorageSpan, randomPointer);
                s.Write(intStorageSpan);
            }
            
            s.Flush();
            return Task.CompletedTask;
        }

        public Task<ListNode> Deserialize(Stream s)
        {
            //Dictionary for nodes sequential id
            var nodesSeqIds = new List<ListNode>();

            //Int value span buffer
            Span<byte> intStorageSpan = stackalloc byte[4];

            // Read data blocks 
            s.Position = 0;
            while (s.Position < s.Length)
            {
                var node = new ListNode();
                s.Read(intStorageSpan);
                var dataLength = BitConverter.ToInt32(intStorageSpan);

                if (dataLength == DataAndPointersSeparator)
                {
                    // Finished reading data part 
                    break;
                }

                if (dataLength == NullMarker)
                {
                    node.Data = null;
                }
                else
                {
                    var dataBytes = _arrayPool.Rent(dataLength);
                    s.Read(dataBytes, 0, dataLength);
                    //Think of a way not to allocate memory twice :C
                    node.Data = Encoding.Unicode.GetString(dataBytes, 0, dataLength);
                    _arrayPool.Return(dataBytes);
                }

                nodesSeqIds.Add(node);
            }

            var currentIndex = 0;
            int randomPointerIndex;
            
            // Read random indexes + set next/prev
            while (s.Position < s.Length)
            {
                var node = nodesSeqIds[currentIndex];
                s.Read(intStorageSpan);
                randomPointerIndex = BitConverter.ToInt32(intStorageSpan);

                node.Previous = currentIndex == 0 ? null : nodesSeqIds[currentIndex - 1];
                
                node.Next = currentIndex == nodesSeqIds.Count - 1 ? null : nodesSeqIds[currentIndex + 1];
                
                if (randomPointerIndex != -1)
                {
                    node.Random = nodesSeqIds[randomPointerIndex];
                }

                currentIndex++;
            }

            return Task.FromResult(nodesSeqIds[0]);
        }
    }
}
