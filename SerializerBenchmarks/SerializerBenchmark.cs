﻿using BenchmarkDotNet.Attributes;
using SerializerDataGenerator;
using SerializerTests.Implementations;
using SerializerTests.Interfaces;
using SerializerTests.Nodes;

namespace SerializerBenchmarks;

[MemoryDiagnoser]
public class SerializerBenchmark
{
    private IListSerializer _serializer;
    private DataGenerator _dataGenerator;
    private ListNode? _testSubject;
    private Stream _stream;
    
    [Params(10, 100, 1000, 10000)]
    public int ListLength { get; set; }
    
    [Params(10, 100, 1000, 10000)]
    public int DataLength { get; set; }
    
    [GlobalSetup]
    public void GlobalSetup()
    {
        _serializer = new MySerializer();
        _dataGenerator = new DataGenerator(null);
    }
    
    [IterationSetup]
    public void IterationSetup()
    {
        _testSubject = _dataGenerator.GenerateRandomNodeList(ListLength, _ => _dataGenerator.GenerateRandomString(DataLength));
        _stream = new MemoryStream();
    }

    [Benchmark]
    public void Benchmark()
    {
        _serializer.Serialize(_testSubject, _stream);
        _serializer.Deserialize(_stream);
    }
    
    [IterationCleanup]
    public void IterationCleanup()
    {
        _testSubject = null;
        _stream.Dispose();
    }
}