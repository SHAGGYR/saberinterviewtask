using SerializerDataGenerator;
using SerializerTests.Implementations;
using SerializerTests.Interfaces;
using SerializerTests.Nodes;

namespace SerializerTests;

public class SerializerTests
{
    private readonly IListSerializer _sut = new MySerializer();
    private readonly DataGenerator _dataGenerator = new(null);
    
    [Test]
    public async Task DeepCopyTest_ShouldMatch()
    {
        var expected = _dataGenerator.GenerateRandomNodeList(10, i => i.ToString());

        var real = await _sut.DeepCopy(expected);
        
        AssertLists(expected, real);
    }
    
    [Test]
    public async Task Serialize_Deserialize_ShouldMatch()
    {
        var expected = _dataGenerator.GenerateRandomNodeList(10, (i => _dataGenerator.GenerateRandomString(i * 50)));
        expected.Data = "❤️❤️❤️❤️";
        
        using var memoryStream = new MemoryStream();
        await _sut.Serialize(expected, memoryStream);
        var result = await _sut.Deserialize(memoryStream);
        
        AssertLists(expected, result);
    }

    private static void AssertLists(ListNode expected, ListNode real)
    {
        while (expected is not null && real is not null)
        {
            Assert.Multiple(() =>
            {
                Assert.That(real.Data, Is.EqualTo(expected.Data));
                Assert.That(real.Random?.Data, Is.EqualTo(expected.Random?.Data));
            });
            expected = expected.Next;
            real = real.Next;
        }

        Assert.Multiple(() =>
        {
            Assert.That(real, Is.Null);
            Assert.That(expected, Is.Null);
        });
    }
}