﻿using SerializerTests.Nodes;

namespace SerializerDataGenerator;

public class DataGenerator
{
    private readonly Random _rand;

    public DataGenerator(int? seed)
    {
        _rand = seed.HasValue ? new Random(seed.Value) : new Random();
    }

    public ListNode GenerateRandomNodeList(int length, Func<int, string> dataGenerationFunction)
    {
        if (length == 0)
        {
            throw new ArgumentException("Invalid list length", nameof(length));
        }
        
        var listNodes = new ListNode[length];
        for (var i = 0; i< length; i++)
        {
            listNodes[i] = new ListNode();
        }

        var randoms = new Stack<int>(Enumerable.Range(0, length).OrderBy(x => _rand.Next()));

        if (length > 2)
        {
            for (var i = 1; i < length - 1; i++)
            {
                var randomEnabled = _rand.Next(10) < 7;
                listNodes[i].Data = dataGenerationFunction(i);
                listNodes[i].Next = listNodes[i + 1];
                listNodes[i].Previous = listNodes[i - 1];
                listNodes[i].Random = randomEnabled 
                    ? listNodes[randoms.Pop()]
                    : null;
            }
        }
        
        listNodes[0].Data = dataGenerationFunction(0);
        listNodes[0].Next = length > 1 ? listNodes[1] : null;
        listNodes[0].Random = listNodes[randoms.Pop()];

        if (length != 1)
        {
            listNodes[length-1].Data = dataGenerationFunction(length-1);
            listNodes[length-1].Previous = listNodes[length-2];
            listNodes[length-1].Random = listNodes[randoms.Pop()]; 
        }
        
        return listNodes[0];
    }

    public string GenerateRandomString(int length)
    {
        var array = new char[length];
        var arraySpan = new Span<char>(array);
        for (var index = 0; index < array.Length; index++)
        {
            arraySpan[index] = (char)_rand.Next(33, 127);
        }

        return arraySpan.ToString();
    }
}